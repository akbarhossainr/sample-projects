@extends('layouts.master')
@section('content')

    <div class="container">
        <div class="col-sm-12">
            <h2 class="text-center">Products List</h2>
        </div>

        <div class="col-sm-12">
            <div class="panel panel-default">
                {{--<div class="panel-heading">Product list</div>--}}
                <div class="panel-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Product title</th>
                            <th>Description</th>
                            <th>Price (BDT)</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($products as $product)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $product->title }}</td>
                            <td>{{ str_limit($product->description, 100, '...') }}</td>
                            <td>{{ $product->price }}</td>
                            <td class="text-center">
                                <a href="{{ route('products.single', $product->id) }}" class="btn btn-primary btn-sm">Details</a>
                                <a href="" class="btn btn-info btn-sm">Edit</a>
                                <a href="" class="btn btn-danger btn-sm">Delete</a>
                            </td>
                        </tr>
                        @empty
                            <tr>
                                <td colspan="5"><h3 class="text-center text-muted">No data found!</h3></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection