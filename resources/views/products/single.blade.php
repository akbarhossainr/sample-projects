@extends('layouts.master')
@section('content')

    <div class="container">
        <div class="col-sm-12">
            <h2 class="text-center">Product Details</h2>
        </div>

        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>Title</td>
                                <td>: {{ $product->title }}</td>
                            </tr>
                            <tr>
                                <td>Description</td>
                                <td>: {{ $product->description }}</td>
                            </tr>
                            <tr>
                                <td>Category</td>
                                <td>: {{ $product->category }}</td>
                            </tr>
                            <tr>
                                <td>Price</td>
                                <td>: {{ $product->price }} BDT</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection