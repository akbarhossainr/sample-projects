<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sample Project</title>

    <link type="text/css" rel="stylesheet" href="{{ url('css/bootstrap.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ url('css/bootstrap.theme.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ url('css/style.css') }}">
</head>
<body>

    {{-- load contents here --}}
    @yield('content')


<script type="text/javascript" src="{{ url('js/jquery-3.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ url('js/bootstrap.min.js') }}"></script>
</body>
</html>