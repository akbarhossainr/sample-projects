@extends('layouts.auth_layout')
@section('content')

    <div class="container">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="box-login-register well">
                <h3 class="text-center">Login</h3>
                <hr>
                @if($errors->has('invalid'))
                <div class="alert alert-danger alert-dismissible text-center" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Warning!</strong> {{ $errors->first('invalid') }}
                </div>
                @endif
                <form class="form-horizontal" method="post" action="{{ route('login.attempt') }}">
                    {{ csrf_field() }}
                    <div class="form-group @if(!empty($errors->has('email'))) has-error @endif">
                        <label for="email" class="col-sm-4 control-label">Email</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="@if(old('email')){{ old('email') }} @endif">
                            @if(!empty($errors->has('email'))) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                        </div>
                    </div>
                    <div class="form-group @if(!empty($errors->has('password'))) has-error @endif">
                        <label for="password" class="col-sm-4 control-label">Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                            @if(!empty($errors->has('password'))) <p class="help-block">{{ $errors->first('password') }}</p> @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-sm-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                            <button type="submit" class="btn btn-primary pull-right">Log in</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection