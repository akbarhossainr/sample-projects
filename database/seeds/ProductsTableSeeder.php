<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                'title' => 'HP Laptop',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque debitis eius excepturi labore minima molestias odio perferendis, placeat quasi quos.',
                'category' => 'Computer',
                'price' => '50000'
            ],
            [
                'title' => 'TP Link Router',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque debitis eius excepturi',
                'category' => 'Router',
                'price' => '1850'
            ],
            [
                'title' => 'MI Band',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque debitis eius excepturi lorem consectetur adipisicing elit. Cumque debitis eius excepturi lorem',
                'category' => 'Accessories',
                'price' => '2000'
            ]
        ]);
    }
}
