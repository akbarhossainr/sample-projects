<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>['guest']], function (){
    Route::get('/', function () {
        return redirect(route('login'));
    });
    Route::get('/login', ['as' => 'login', 'uses'=>'UserController@viewLogin']);
    Route::post('/login', ['as' => 'login.attempt', 'uses'=>'UserController@doLogin']);

});

Route::get('/logout', ['as' => 'logout', 'uses'=>'UserController@logout']);

Route::group(['middleware'=>['web','auth']], function (){
    Route::get('/products', ['as' => 'products.list', 'uses'=>'ProductController@index']);
    Route::get('/products/{id}', ['as' => 'products.single', 'uses'=>'ProductController@show']);
});
