<?php

namespace App\Repositories;

use App\Product;

/**
 * Created by PhpStorm.
 * User: ahr
 * Date: 7/27/17
 * Time: 12:48 AM
 */
class ProductRepository
{

    protected $product;
    public function __construct(Product $product)
    {
        $this->product = $product;
    }
    public function all()
    {
        return $this->product->latest()->get();
    }

    public function single($id)
    {
        return $this->product->find($id);
    }
}