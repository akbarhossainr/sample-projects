<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /*
     * view login page
     */
    public function viewLogin()
    {
        return view('auth.login');
    }

    public function doLogin(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $credentials = $request->only('email','password');

        if (Auth::attempt($credentials, $request->has('remember'))){
            return redirect(route('products.list'));
        }

        return redirect()->back()->withInput($request->only('email','remember'))->withErrors(['invalid' => 'Username or password do not match.']);
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

}
