<?php

namespace App\Http\Controllers;

use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    /*
     * using Repository pattern
     */
    protected $productRepository;
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /*
     * view product lists
     */
    public function index()
    {
        $products =  $this->productRepository->all();
        return view('products.list')->with(compact('products'));
    }

    /*
     * view single product
     */
    public function show($id)
    {
        $product = $this->productRepository->single($id);
        if (!!$product)
            return view('products.single')->with(compact('product'));

        return redirect('/');
    }
}
